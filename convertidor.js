
const archivojson = {
    planeamiento:0,
    id_ciudad:76001,
    festivos:["2020-03-23","2020-04-05","2020-04-09","2020-04-10","2020-04-12","2020-05-01","2020-05-25","2020-06-15","2020-06-22","2020-06-29","2020-07-20","2020-08-07","1999-03-23","1999-04-05","1999-04-09","1999-04-10","1999-04-12","1999-05-01","1999-05-25","1999-06-15","1999-06-22","1999-06-29","1999-07-20","1999-08-07","2000-03-23","2000-04-05","2000-04-09","2000-04-10","2000-04-12","2000-05-01","2000-05-25","2000-06-15","2000-06-22","2000-06-29","2000-07-20","2000-08-07","2020-08-17","2020-10-12","2020-11-02","2020-11-16"],
    timestamp:978325424,
    cruces:[
        {plan_conexion:false,
        grupos:[
                {rojo_error:[false,0],
                    amarillo_error:[false,0],
                    tipo:"TipoGrupo.Vehicular",
                    amarillo_fundido:[false,0],
                    tv_conexion:0,
                    coordenadas:[],
                    verde_fundido:[false,0],
                    activo:false,
                    rojo_fundido:[false,0],
                    numero:0,
                    verde_error:[false,0],
                    nombre:"",
                    color_destello:"Color.Destello_amarillo",
                    flujo:-1},
                {rojo_error:[false,0],
                    amarillo_error:[false,0],
                    tipo:"TipoGrupo.Vehicular",
                    amarillo_fundido:[false,0],
                    tv_conexion:0,
                    coordenadas:[],
                    verde_fundido:[false,0],
                    activo:false,
                    rojo_fundido:[false,0],
                    numero:1,
                    verde_error:[false,0],
                    nombre:"",
                    color_destello:"Color.Destello_amarillo",
                    flujo:-1},
                {rojo_error:[false,0],
                    amarillo_error:[false,0],
                    tipo:"TipoGrupo.Vehicular",
                    amarillo_fundido:[false,0],
                    tv_conexion:0,
                    coordenadas:[],
                    verde_fundido:[false,0],
                    activo:false,
                    rojo_fundido:[false,0],
                    numero:2,
                    verde_error:[false,0],
                    nombre:"",
                    color_destello:"Color.Destello_amarillo",
                    flujo:-1},
                    {rojo_error:[false,0],
                        amarillo_error:[false,0],
                        tipo:"TipoGrupo.Vehicular",
                        amarillo_fundido:[false,0],
                        tv_conexion:0,
                        coordenadas:[],
                        verde_fundido:[false,0],
                        activo:false,
                        rojo_fundido:[false,0],
                        numero:3,
                        verde_error:[false,0],
                        nombre:"",
                        color_destello:"Color.Destello_amarillo",
                        flujo:-1},
                        {rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:4,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},
                        {rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:5,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},
                        {rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:6,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:7,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:8,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:9,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:10,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:11,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:12,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:13,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:14,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:15,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:16,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:17,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:18,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:19,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:20,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:21,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:22,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:23,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Peatonal",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:24,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:25,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_amarillo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:26,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:27,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_amarillo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:28,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:29,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:30,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1},{rojo_error:[false,0],amarillo_error:[false,0],tipo:"TipoGrupo.Vehicular",amarillo_fundido:[false,0],tv_conexion:0,coordenadas:[],verde_fundido:[false,0],activo:false,rojo_fundido:[false,0],numero:31,verde_error:[false,0],nombre:"",color_destello:"Color.Destello_rojo",flujo:-1}],
        modo_operacion:"ModoOperacion.Autocontrol",
        numero:0,
        direccion:"",
        estructuras:[
            {TRA:2,
            tiempos_intermedios:
                [{grupo2:1,grupo1:0,tiempo:6,numero:0},
                {grupo2:2,grupo1:0,tiempo:6,numero:1},
                {grupo2:3,grupo1:0,tiempo:6,numero:2},
                {grupo2:0,grupo1:1,tiempo:6,numero:3},
                {grupo2:2,grupo1:1,tiempo:6,numero:4},
                {grupo2:3,grupo1:1,tiempo:6,numero:5},
                {grupo2:0,grupo1:2,tiempo:6,numero:6},
                {grupo2:1,grupo1:2,tiempo:6,numero:7},
                {grupo2:3,grupo1:2,tiempo:6,numero:8},
                {grupo2:0,grupo1:3,tiempo:6,numero:9},{grupo2:1,grupo1:3,tiempo:6,numero:10},
                {grupo2:2,grupo1:3,tiempo:6,numero:11},{grupo2:0,grupo1:4,tiempo:6,numero:12},
                {grupo2:4,grupo1:0,tiempo:6,numero:13},{grupo2:1,grupo1:4,tiempo:6,numero:14},
                {grupo2:4,grupo1:1,tiempo:6,numero:15},{grupo2:2,grupo1:4,tiempo:6,numero:16},
                {grupo2:4,grupo1:2,tiempo:6,numero:17},{grupo2:3,grupo1:4,tiempo:6,numero:18},
                {grupo2:4,grupo1:3,tiempo:6,numero:19},{grupo2:0,grupo1:5,tiempo:6,numero:20},
                {grupo2:5,grupo1:0,tiempo:6,numero:21},{grupo2:1,grupo1:5,tiempo:6,numero:22},
                {grupo2:5,grupo1:1,tiempo:6,numero:23},{grupo2:2,grupo1:5,tiempo:6,numero:24},
                {grupo2:5,grupo1:2,tiempo:6,numero:25},{grupo2:3,grupo1:5,tiempo:6,numero:26},
                {grupo2:5,grupo1:3,tiempo:6,numero:27},{grupo2:4,grupo1:5,tiempo:6,numero:28},
                {grupo2:5,grupo1:4,tiempo:6,numero:29}],
    grupos_amigos:[{grupo2:1,grupo1:0},
        {grupo2:2,grupo1:0},{grupo2:3,grupo1:0},
        {grupo2:11,grupo1:0},{grupo2:13,grupo1:0},
        {grupo2:14,grupo1:0},{grupo2:18,grupo1:0},
        {grupo2:19,grupo1:0},{grupo2:20,grupo1:0},
        {grupo2:21,grupo1:0},{grupo2:22,grupo1:0},
        {grupo2:23,grupo1:0},{grupo2:24,grupo1:0},
        {grupo2:2,grupo1:1},{grupo2:3,grupo1:1},
        {grupo2:11,grupo1:1},{grupo2:13,grupo1:1},
        {grupo2:14,grupo1:1},{grupo2:18,grupo1:1},
        {grupo2:19,grupo1:1},{grupo2:20,grupo1:1},{grupo2:21,grupo1:1},{grupo2:22,grupo1:1},{grupo2:23,grupo1:1},{grupo2:24,grupo1:1},{grupo2:3,grupo1:2},{grupo2:11,grupo1:2},{grupo2:13,grupo1:2},{grupo2:14,grupo1:2},{grupo2:18,grupo1:2},{grupo2:19,grupo1:2},{grupo2:20,grupo1:2},{grupo2:21,grupo1:2},{grupo2:22,grupo1:2},{grupo2:23,grupo1:2},{grupo2:24,grupo1:2},{grupo2:11,grupo1:3},{grupo2:13,grupo1:3},{grupo2:14,grupo1:3},{grupo2:18,grupo1:3},{grupo2:19,grupo1:3},{grupo2:20,grupo1:3},{grupo2:21,grupo1:3},{grupo2:22,grupo1:3},{grupo2:23,grupo1:3},{grupo2:24,grupo1:3},{grupo2:5,grupo1:4},{grupo2:6,grupo1:4},{grupo2:7,grupo1:4},{grupo2:9,grupo1:4},{grupo2:10,grupo1:4},{grupo2:17,grupo1:4},{grupo2:6,grupo1:5},{grupo2:7,grupo1:5},{grupo2:9,grupo1:5},{grupo2:10,grupo1:5},{grupo2:17,grupo1:5},{grupo2:7,grupo1:6},{grupo2:9,grupo1:6},{grupo2:10,grupo1:6},{grupo2:17,grupo1:6},{grupo2:9,grupo1:7},{grupo2:10,grupo1:7},{grupo2:17,grupo1:7},{grupo2:10,grupo1:9},{grupo2:11,grupo1:9},{grupo2:12,grupo1:9},{grupo2:13,grupo1:9},{grupo2:14,grupo1:9},{grupo2:15,grupo1:9},{grupo2:16,grupo1:9},{grupo2:17,grupo1:9},{grupo2:11,grupo1:10},{grupo2:12,grupo1:10},{grupo2:13,grupo1:10},{grupo2:14,grupo1:10},{grupo2:15,grupo1:10},{grupo2:16,grupo1:10},{grupo2:17,grupo1:10},{grupo2:12,grupo1:11},{grupo2:13,grupo1:11},{grupo2:14,grupo1:11},{grupo2:15,grupo1:11},{grupo2:16,grupo1:11},{grupo2:17,grupo1:11},{grupo2:18,grupo1:11},{grupo2:19,grupo1:11},{grupo2:20,grupo1:11},{grupo2:21,grupo1:11},{grupo2:22,grupo1:11},{grupo2:23,grupo1:11},{grupo2:24,grupo1:11},{grupo2:13,grupo1:12},{grupo2:14,grupo1:12},{grupo2:15,grupo1:12},{grupo2:16,grupo1:12},{grupo2:17,grupo1:12},{grupo2:14,grupo1:13},{grupo2:15,grupo1:13},{grupo2:16,grupo1:13},{grupo2:17,grupo1:13},{grupo2:18,grupo1:13},{grupo2:19,grupo1:13},{grupo2:20,grupo1:13},{grupo2:21,grupo1:13},{grupo2:22,grupo1:13},{grupo2:23,grupo1:13},{grupo2:24,grupo1:13},{grupo2:15,grupo1:14},{grupo2:16,grupo1:14},{grupo2:17,grupo1:14},{grupo2:18,grupo1:14},{grupo2:19,grupo1:14},{grupo2:20,grupo1:14},{grupo2:21,grupo1:14},{grupo2:22,grupo1:14},{grupo2:23,grupo1:14},{grupo2:24,grupo1:14},{grupo2:16,grupo1:15},{grupo2:17,grupo1:15},{grupo2:17,grupo1:16},{grupo2:19,grupo1:18},{grupo2:20,grupo1:18},{grupo2:21,grupo1:18},{grupo2:22,grupo1:18},{grupo2:23,grupo1:18},{grupo2:24,grupo1:18},{grupo2:20,grupo1:19},{grupo2:21,grupo1:19},{grupo2:22,grupo1:19},{grupo2:23,grupo1:19},{grupo2:24,grupo1:19},{grupo2:21,grupo1:20},{grupo2:22,grupo1:20},{grupo2:23,grupo1:20},{grupo2:24,grupo1:20},{grupo2:22,grupo1:21},{grupo2:23,grupo1:21},{grupo2:24,grupo1:21},{grupo2:23,grupo1:22},{grupo2:24,grupo1:22},{grupo2:24,grupo1:23}],
    numero:0,
    planes:[
        {senales:[],
            SS:10,
            DC:90,
            numero:0,
            tiempos:[
            {TIRA:80,TIV:82,TFA:25,numero:0,TFRA:82,grupo:0,TFV:22,TIA:22},
            {TIRA:80,TIV:82,TFA:25,numero:1,TFRA:82,grupo:1,TFV:22,TIA:22},
            {TIRA:80,TIV:82,TFA:25,numero:2,TFRA:82,grupo:2,TFV:22,TIA:22},
            {TIRA:80,TIV:82,TFA:25,numero:3,TFRA:82,grupo:3,TFV:22,TIA:22},
            {TIRA:55,TIV:57,TFA:77,numero:4,TFRA:57,grupo:4,TFV:74,TIA:74},
            {TIRA:52,TIV:54,TFA:74,numero:5,TFRA:54,grupo:5,TFV:71,TIA:71},
            {TIRA:55,TIV:57,TFA:77,numero:6,TFRA:57,grupo:6,TFV:74,TIA:74},
            {TIRA:52,TIV:54,TFA:74,numero:7,TFRA:54,grupo:7,TFV:71,TIA:71},
            {TIRA:30,TIV:30,TFA:71,numero:8,TFRA:30,grupo:9,TFV:68,TIA:68},
            {TIRA:30,TIV:30,TFA:71,numero:9,TFRA:30,grupo:10,TFV:68,TIA:68},
            {TIRA:84,TIV:84,TFA:48,numero:10,TFRA:84,grupo:11,TFV:45,TIA:45},
            {TIRA:30,TIV:30,TFA:50,numero:11,TFRA:30,grupo:12,TFV:47,TIA:47},
            {TIRA:84,TIV:84,TFA:50,numero:12,TFRA:84,grupo:13,TFV:47,TIA:47},
            {TIRA:84,TIV:84,TFA:48,numero:13,TFRA:84,grupo:14,TFV:45,TIA:45},
            {TIRA:30,TIV:30,TFA:50,numero:14,TFRA:30,grupo:15,TFV:47,TIA:47},
            {TIRA:30,TIV:30,TFA:50,numero:15,TFRA:30,grupo:16,TFV:47,TIA:47},
            {TIRA:30,TIV:30,TFA:71,numero:16,TFRA:30,grupo:17,TFV:68,TIA:68},
            {TIRA:0,TIV:0,TFA:13,numero:17,TFRA:0,grupo:18,TFV:10,TIA:10},
            {TIRA:0,TIV:0,TFA:13,numero:18,TFRA:0,grupo:19,TFV:10,TIA:10},
            {TIRA:0,TIV:0,TFA:13,numero:19,TFRA:0,grupo:20,TFV:10,TIA:10},
            {TIRA:0,TIV:0,TFA:13,numero:20,TFRA:0,grupo:21,TFV:10,TIA:10},
            {TIRA:0,TIV:0,TFA:13,numero:21,TFRA:0,grupo:22,TFV:10,TIA:10},
            {TIRA:0,TIV:0,TFA:13,numero:22,TFRA:0,grupo:23,TFV:10,TIA:10},
            {TIRA:0,TIV:0,TFA:13,numero:23,TFRA:0,grupo:24,TFV:10,TIA:10},
            {TIRA:0,TIV:2,TFA:15,numero:24,TFRA:2,grupo:8,TFV:12,TIA:12},
            {TIRA:27,TIV:29,TFA:42,numero:25,TFRA:29,grupo:25,TFV:39,TIA:39},
            {TIRA:0,TIV:2,TFA:42,numero:26,TFRA:2,grupo:26,TFV:39,TIA:39},
            {TIRA:27,TIV:29,TFA:42,numero:27,TFRA:29,grupo:27,TFV:39,TIA:39},
            {TIRA:27,TIV:29,TFA:42,numero:28,TFRA:29,grupo:28,TFV:39,TIA:39},
            {TIRA:27,TIV:29,TFA:42,numero:29,TFRA:29,grupo:29,TFV:39,TIA:39},
            {TIRA:27,TIV:29,TFA:42,numero:30,TFRA:29,grupo:30,TFV:39,TIA:39},
            {TIRA:27,TIV:29,TFA:42,numero:31,TFRA:29,grupo:31,TFV:39,TIA:39}],
            retencion:2,
            nombre:"Plan 1 str 1",
            SC:2,horarios:[
                    {timestamp_inicio:-1,
                    timestamp_fin:-1,
                    numero:0,
                    hora_inicio:0,
                    nombre:"",
                    minuto_inicio:0,
                    hora_fin:23,
                    dias:[
                        "Semana.Jueves","Semana.Viernes","Semana.Sabado","Semana.Festivo","Semana.Domingo","Semana.Lunes","Semana.Martes","Semana.Miercoles"],
                        minuto_fin:59}],
                        max_vehiculos:40}],
            nombre:"Estructura 1",
            activa:true,
            TA:3},],
            activo:true,
            coordenadas:[],
            nombre:"",
            codigo:2121,
            estado:"EstadoCruce.Encendido",
                            frecuencia:2}],
            coordenadas:"",
            direccion:"Calle 72 X Via40",
            servidor:"baq.rts.com.co",
            nombre:"7240",
            id_depto:"30",
            codigo:"3003"};


function save()
{
//----------------------------------declaraciones OCIT-------------------------------------------------------------------------------------

StringDeclaraciones1 =  
'<?xml version="1.0"?>'+'\n'+
'   <DSCronos>'+'\n'+
'       <xs:schema id="DSCronos" targetNamespace="http://tempuri.org/DSCronos.xsd" xmlns:mstns="http://tempuri.org/DSCronos.xsd" xmlns="http://tempuri.org/DSCronos.xsd" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" attributeFormDefault="qualified" elementFormDefault="qualified">'+'\n'+
'           <xs:element name="DSCronos" msdata:IsDataSet="true" msdata:Locale="en-US">'+'\n'+
'               <xs:complexType>'+'\n'+
'                   <xs:choice minOccurs="0" maxOccurs="unbounded">'+'\n'+
'                       <xs:element name="Cruce">'+'\n'+
'                           <xs:complexType>'+'\n'+
'                               <xs:sequence>'+'\n'+
'                               <xs:element name="ID_Cruce" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                               <xs:element name="No" type="xs:int" minOccurs="0" />'+'\n'+
'                               <xs:element name="Nombre" type="xs:string" minOccurs="0" />'+'\n'+
'                               <xs:element name="ID_ModOperacion" type="xs:int" minOccurs="0" />'+'\n'+
'                               <xs:element name="Dir" type="xs:string" minOccurs="0" />'+'\n'+
'                               <xs:element name="ID_Equipo" type="xs:int" minOccurs="0" />'+'\n'+
'                               <xs:element name="FD" type="xs:double" minOccurs="0" />'+'\n'+
'                               <xs:element name="Cod" type="xs:int" minOccurs="0" />'+'\n'+
'                               <xs:element name="Activo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                               <xs:element name="PlanConexion" type="xs:boolean" default="false" />'+'\n'+
'                               </xs:sequence>'+'\n'+
'                           </xs:complexType>'+'\n'+
'                       </xs:element>'+'\n'+
'                   <xs:element name="GrupoAmigo">'+'\n'+
'            <xs:complexType>'+'\n'+
'              <xs:sequence>'+'\n'+
'                 <xs:element name="ID_GrupoAmigo" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="ID_Grupo1" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Grupo2" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Estructura" type="xs:int" minOccurs="0" />'+'\n'+
'              </xs:sequence>'+'\n'+
'            </xs:complexType>'+'\n'+
'          </xs:element>'+'\n'
let stringDeclaraciones2 =
'          <xs:element name="Grupo">'+'\n'+
'            <xs:complexType>'+'\n'+
'              <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Grupo" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="No" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Nombre" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="DestelloRojo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="DestelloAmarillo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Cruce" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_TipoGrupo" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TVConexion" type="xs:int" default="0" />'+'\n'+
'                 <xs:element name="FlujoActivo" type="xs:boolean" default="false" minOccurs="0" />'+'\n'+
'                 <xs:element name="Flujo" type="xs:int" default="-1" minOccurs="0" />'+'\n'+
'                 <xs:element name="TipoFlujo" type="xs:boolean" default="false" minOccurs="0" />'+'\n'+
'                 <xs:element name="ValorMaximo" type="xs:int" default="0" minOccurs="0" />'+'\n'+
'                 <xs:element name="ValorMinimo" type="xs:int" default="0" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Plan" type="xs:int" default="-1" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Grupo" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="No" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Nombre" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="DestelloRojo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="DestelloAmarillo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Cruce" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_TipoGrupo" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TVConexion" type="xs:int" default="0" />'+'\n'+
'                 <xs:element name="FlujoActivo" type="xs:boolean" default="false" minOccurs="0" />'+'\n'+
'                 <xs:element name="Flujo" type="xs:int" default="-1" minOccurs="0" />'+'\n'+
'                 <xs:element name="TipoFlujo" type="xs:boolean" default="false" minOccurs="0" />'+'\n'+
'                 <xs:element name="ValorMaximo" type="xs:int" default="0" minOccurs="0" />'+'\n'+
'                 <xs:element name="ValorMinimo" type="xs:int" default="0" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Plan" type="xs:int" default="-1" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Estado">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Estado" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="TIRA" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TFRA" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TIV" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TFV" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TIA" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TFA" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Plan" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Grupo" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Plan">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Plan" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="No" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Nombre" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="SS" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="SC" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Estructura" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="DC" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="MaxVehiculos" type="xs:int" default="0" />'+'\n'+
'                 <xs:element name="Retardo" type="xs:int" default="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Senales">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Senales" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="Parada" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Avance" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Plan" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Equipo">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Equipo" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="Cod" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Nombre" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="IP_Central" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Depto" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Ciudad" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Dir" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="ClaveTiempo" type="xs:dateTime" minOccurs="0" />'+'\n'+
'                 <xs:element name="MiniCova" type="xs:boolean" default="false" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="HVentilador">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_HVentilador" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="H_Inicio" type="xs:dateTime" minOccurs="0" />'+'\n'+
'                 <xs:element name="H_Fin" type="xs:dateTime" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Equipo" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="HPlan">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_HPlan" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="H_Inicio" type="xs:dateTime" minOccurs="0" />'+'\n'+
'                 <xs:element name="H_Fin" type="xs:dateTime" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Plan" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Lunes" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Martes" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Miercoles" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Jueves" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Viernes" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Sabado" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Domingo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="Festivo" type="xs:boolean" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="TIntermedio">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_TIntermedio" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="ID_Grupo1" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Grupo2" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Tiempo" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="BTiempo" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Estructura" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'          </xs:element>'+'\n'+
'           <xs:element name="Estructura">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Estructura" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="No" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Nombre" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="TRA" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="BTRA" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="TA" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="BTA" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Cruce" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Evento">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Evento" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />'+'\n'+
'                 <xs:element name="CodEquipo" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="CodCruce" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="TipoEvento" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="NoGrupo1" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="NoGrupo2" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="FechaHora" type="xs:dateTime" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Equipo" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Configuracion">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="Version" type="xs:string" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="GrupoInventario">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Tarjeta" msdata:AutoIncrement="true" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Presente" type="xs:boolean" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Equipo" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="Grupos" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="PrimerGrupo" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="DispostivoBT">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_DispostivoBT" msdata:AutoIncrement="true" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="NombreBT" type="xs:string" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Equipo" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'           <xs:element name="Contador">'+'\n'+
'             <xs:complexType>'+'\n'+
'               <xs:sequence>'+'\n'+
'                 <xs:element name="ID_Contador" msdata:AutoIncrement="true" type="xs:int" minOccurs="0" />'+'\n'+
'                 <xs:element name="No" type="xs:int" default="-1" minOccurs="0" />'+'\n'+
'                 <xs:element name="Positivo" type="xs:boolean" default="false" minOccurs="0" />'+'\n'+
'                 <xs:element name="ID_Grupo" type="xs:int" minOccurs="0" />'+'\n'+
'               </xs:sequence>'+'\n'+
'             </xs:complexType>'+'\n'+
'           </xs:element>'+'\n'+
'         </xs:choice>'+'\n'+
'         </xs:complexType>'+'\n'+
'         <xs:unique name="DSCronosKey1" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Cruce" />'+'\n'+
'         <xs:field xpath="mstns:ID_Cruce" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey8" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:GrupoAmigo" />'+'\n'+
'         <xs:field xpath="mstns:ID_GrupoAmigo" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey3" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Grupo" />'+'\n'+
'         <xs:field xpath="mstns:ID_Grupo" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey4" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Estado" />'+'\n'+
'         <xs:field xpath="mstns:ID_Estado" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey2" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Plan" />'+'\n'+
'         <xs:field xpath="mstns:ID_Plan" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey7" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Senales" />'+'\n'+
'         <xs:field xpath="mstns:ID_Senales" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey9" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Equipo" />'+'\n'+
'         <xs:field xpath="mstns:ID_Equipo" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey10" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:HVentilador" />'+'\n'+
'         <xs:field xpath="mstns:ID_HVentilador" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey5" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:HPlan" />'+'\n'+
'         <xs:field xpath="mstns:ID_HPlan" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey6" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:TIntermedio" />'+'\n'+
'         <xs:field xpath="mstns:ID_TIntermedio" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey11" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Estructura" />'+'\n'+
'         <xs:field xpath="mstns:ID_Estructura" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DSCronosKey12" msdata:PrimaryKey="true">'+'\n'+
'         <xs:selector xpath=".//mstns:Evento" />'+'\n'+
'         <xs:field xpath="mstns:ID_Evento" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="DispostivoBTKey1">'+'\n'+
'         <xs:selector xpath=".//mstns:DispostivoBT" />'+'\n'+
'         <xs:field xpath="mstns:ID_DispostivoBT" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:unique name="ContadorKey1">'+'\n'+
'         <xs:selector xpath=".//mstns:Contador" />'+'\n'+
'         <xs:field xpath="mstns:ID_Contador" />'+'\n'+
'       </xs:unique>'+'\n'+
'       <xs:keyref name="EquipoEvento" refer="DSCronosKey9">'+'\n'+
'         <xs:selector xpath=".//mstns:Evento" />'+'\n'+
'         <xs:field xpath="mstns:ID_Equipo" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="CruceEstructura" refer="DSCronosKey1">'+'\n'+
'         <xs:selector xpath=".//mstns:Estructura" />'+'\n'+
'         <xs:field xpath="mstns:ID_Cruce" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="EstructuraTIntermedio" refer="DSCronosKey11">'+'\n'+
'         <xs:selector xpath=".//mstns:TIntermedio" />'+'\n'+
'         <xs:field xpath="mstns:ID_Estructura" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="PlanHPlan" refer="DSCronosKey2">'+'\n'+
'         <xs:selector xpath=".//mstns:HPlan" />'+'\n'+
'         <xs:field xpath="mstns:ID_Plan" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="EquipoHVentilador" refer="DSCronosKey9">'+'\n'+
'         <xs:selector xpath=".//mstns:HVentilador" />'+'\n'+
'         <xs:field xpath="mstns:ID_Equipo" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="PlanSenales" refer="DSCronosKey2">'+'\n'+
'         <xs:selector xpath=".//mstns:Senales" />'+'\n'+
'         <xs:field xpath="mstns:ID_Plan" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="EstructuraPlan" refer="DSCronosKey11">'+'\n'+
'         <xs:selector xpath=".//mstns:Plan" />'+'\n'+
'         <xs:field xpath="mstns:ID_Estructura" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="GrupoEstado" refer="DSCronosKey3">'+'\n'+
'         <xs:selector xpath=".//mstns:Estado" />'+'\n'+
'         <xs:field xpath="mstns:ID_Grupo" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="PlanEstado" refer="DSCronosKey2">'+'\n'+
'         <xs:selector xpath=".//mstns:Estado" />'+'\n'+
'         <xs:field xpath="mstns:ID_Plan" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="CruceGrupo" refer="DSCronosKey1">'+'\n'+
'         <xs:selector xpath=".//mstns:Grupo" />'+'\n'+
'         <xs:field xpath="mstns:ID_Cruce" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="EstructuraGrupoAmigo" refer="DSCronosKey11">'+'\n'+
'         <xs:selector xpath=".//mstns:GrupoAmigo" />'+'\n'+
'         <xs:field xpath="mstns:ID_Estructura" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       <xs:keyref name="EquipoCruce" refer="DSCronosKey9">'+'\n'+
'         <xs:selector xpath=".//mstns:Cruce" />'+'\n'+
'         <xs:field xpath="mstns:ID_Equipo" />'+'\n'+
'       </xs:keyref>'+'\n'+
'       </xs:element>'+'\n'+
'       <xs:annotation>'+'\n'+
'       <xs:appinfo>'+'\n'+
'         <msdata:Relationship name="Equipo_GrupoInventario" msdata:parent="Equipo" msdata:child="GrupoInventario" msdata:parentkey="ID_Equipo" msdata:childkey="ID_Equipo" />'+'\n'+
'         <msdata:Relationship name="Equipo_DispostivoBT" msdata:parent="Equipo" msdata:child="DispostivoBT" msdata:parentkey="ID_Equipo" msdata:childkey="ID_Equipo" />'+'\n'+
'         <msdata:Relationship name="Grupo_Contador" msdata:parent="Grupo" msdata:child="Contador" msdata:parentkey="ID_Grupo" msdata:childkey="ID_Grupo" />'+'\n'+
'         <msdata:Relationship name="Grupo_Plan" msdata:parent="Plan" msdata:child="Grupo" msdata:parentkey="ID_Plan" msdata:childkey="ID_Plan" />'+'\n'+
'       </xs:appinfo>'+'\n'+
'     </xs:annotation>'+'\n'+
'     </xs:schema>'+'\n'+
'     <diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"></DSCronos>'+'\n'+
'     <DSCronos xmlns="http://tempuri.org/DSCronos.xsd">';
// //----------------------------------------------Cruces--------------------------------------------------------------

    let matrizCruces = archivojson.cruces;
    let bufferCruces = ''
    var i=0;
    matrizCruces.forEach(()=>{
        bufferCruces = 
        '<Cruce diffgr:id="Cruce2" msdata:rowOrder="0" diffgr:hasChanges="inserted">'+'\n'+
            '<ID_Cruce>'+matrizCruces[i].codigo+'</ID_Cruce>'+'\n'+
            '<No>'+matrizCruces[i].numero+'</No>'+'\n'+
            '<Nombre>'+matrizCruces[i].nombre+'</Nombre>'+'\n'+
            '<ID_ModOperacion>'+matrizCruces[i].modo_operacion+'</ID_ModOperacion>'+'\n'+
            '<Dir />'+'\n'+//verificar
            '<ID_Equipo>'+archivojson.nombre+'</ID_Equipo>'+'\n'+
            '<FD>500</FD>'+'\n'+//verificar
            '<Cod>'+archivojson.codigo+'</Cod>'+'\n'+
            '<Activo>'+matrizCruces[i].activo+'</Activo>'+'\n'+
            '<PlanConexion>'+matrizCruces[i].plan_conexion+'</PlanConexion>'+'\n'+
      '</Cruce>'
        
    });
    stringCruces = bufferCruces.toString();
//-------------------------------------------Grupos Amigos------------------------------------------------
    var x =0;
    var i =0;
    var j =0;
    var k =0;
    let matrizGruposAmigos= archivojson.cruces[i].estructuras[j].grupos_amigos;
    let matrizEstructura= archivojson.cruces[i].estructuras;
    let bufferGrupoAmigo = []
    //recorrido a través de los cruces
        matrizCruces.forEach(()=>{
            //Recorrido a través de las diferentes estructuras
            matrizEstructura.forEach( () =>{
            //Recorrido a tráves de la matriz de grupos amigos para armar los identificadores para OCIT
                matrizGruposAmigos.forEach(() =>{
                    bufferGrupoAmigo[k] =
                    '<GrupoAmigo diffgr:id"GrupoAmigo'+(k+1)+'" msdata:rowOrder="'+k+'" diffgr:hasChanges="inserted">'+'\n'+
                    '<ID_GrupoAmigo>'+i+'</ID_GrupoAmigos>'+'\n'+
                    '<ID_Grupo1>'+archivojson.cruces[i].estructuras[j].grupos_amigos[k].grupo1+'</ID_Grupo1>'+'\n'+
                    '<ID_Grupo2>'+archivojson.cruces[i].estructuras[j].grupos_amigos[k].grupo2+'</ID_grupo2>'+'\n'+
                    '<ID_Estructura>'+matrizEstructura[j].numero+'</ID_Estructura>'+'\n'+
                    '</GrupoAmigo>'+'\n';
                    k=k+1;
                    //console.log(bufferGrupoAmigo);
                });
                j=j+1;
            });
            i=i+1;
        });

        stringGruposAmigos = bufferGrupoAmigo.toString();
//-------------------------------------------------------Grupos----------------------------------------------------------------

    var i =0;
    var j =0;
    var k =0;
    var l =0;
    var m =0;
    let bufferGrupos = [];
    let matrizGrupos= archivojson.cruces[i].grupos;
matrizCruces.forEach(()=>{
    matrizEstructura.forEach(()=>{
    matrizGrupos.forEach(()=>{
        //Discriminación de destellos rojo o amarillo
        scolorDestello = matrizGrupos[k].color_destello.split('.');
        if(scolorDestello[1]==="Destello_amarillo"){
            destelloRojo = false;
            destelloAmarillo=true;
          }
            else{
                destelloRojo=true;
                destelloAmarillo=false;
        }

        //discriminación del tipo de grupos
        stipoGrupo = matrizGrupos[k].tipo.split('.');
        if(stipoGrupo[1]==="Vehicular"){
            tipo="0";
        }else{
            tipo="1";
        }
        //depuración de datos en la matriz de planes



        bufferGrupos[k] =
        '<Grupo diffgr:id="Grupo'+matrizGrupos[k].numero+1+'" msdata:rowOrder="'+k+'" diffgr:hasChanges="inserted">'+'\n'+
        '<ID_Grupo>'+(matrizGrupos[k].numero+1)+'</ID_Grupo>'+'\n'+
        '<No>'+matrizGrupos[k].numero+'</No>'+'\n'+
        '<Nombre>Grupo'+(k+1)+'</Nombre>'+'\n'+
        '<DestelloRojo>'+destelloRojo+'</DestelloRojo>'+'\n'+
        '<DestelloAmarillo>'+destelloAmarillo+'</DestelloAmarillo>'+'\n'+
        '<ID_Cruce>'+archivojson.cruces[i].codigo+'</ID_Cruce>'+'\n'+
        '<ID_TipoGrupo>'+tipo+'</ID_TipoGrupo>'+'\n'+
        '<TVConexion>'+matrizGrupos[k].tv_conexion+'</TVConexion>'+'\n'+
        '<FlujoActivo>'+matrizGrupos[k].activo+'</FlujoActivo>'+'\n'+
        '<Flujo>'+matrizGrupos[k].flujo+'</Flujo>'+'\n'+
        '<TipoFlujo>false</TipoFlujo>'+'\n'+//Revisar esta parte
        '<ValorMaximo>0</ValorMaximo>'+'\n'+//
        '<ValorMinimo>0</ValorMinimo>'+'\n'+//
        '<ID_Plan>'+matrizEstructura[j].numero+'</ID_Plan>'+'\n'+
        '</Grupo>'+'\n';
        k=k+1;


    });
    j=j+1;

})
i=i+1;
})

let stringGrupos= bufferGrupos.toString();


//---------------------------------------------------------Estados (planes)----------------------------------------------------------


      i=0;
      j=0;
      k=0;
      l=0;
      let bufferPlanes = [];
      var matrizPlanes= archivojson.cruces[i].estructuras[j].planes
      var matrizTiempos= archivojson.cruces[i].estructuras[j].planes[k].tiempos
      matrizCruces.forEach(()=>{
        matrizEstructura.forEach(()=>{
            matrizPlanes.forEach(()=>{
                matrizTiempos.forEach(()=>{
                    bufferPlanes[l] = '<Estado diffgr:id="Estado'+(matrizTiempos[l].numero)+'" msdata:rowOrder="'+matrizTiempos[l].numero+'" diffgr:hasChanges="inserted">'+'\n'+
                    '<ID_Estado>'+matrizTiempos[l].numero+'</ID_Estado>'+'\n'+
                    '<TIRA>'+matrizTiempos[l].TIRA+'</TIRA>'+'\n'+
                    '<TFRA>'+matrizTiempos[l].TFRA+'</TFRA>'+'\n'+
                    '<TIV>'+matrizTiempos[l].TIV+'</TIV>'+'\n'+
                    '<TFV>'+matrizTiempos[l].TFV+'</TFV>'+'\n'+
                    '<TIA>'+matrizTiempos[l].TIA+'</TIA>'+'\n'+
                    '<TFA>'+matrizTiempos[l].TFA+'</TFA>'+'\n'+
                    '<ID_Plan>'+matrizEstructura[j].numero+'</ID_Plan>'+'\n'+
                    '<ID_Grupo>'+matrizTiempos[l].grupo+'</ID_Grupo>'+'\n'+
                    '</Estado>'+'\n'
                    l=l+1;
                });
            k=k+1;
            });
        j=j+1
        });
    i=i=+1
    });
    stringTPlanes = bufferPlanes.toString();
    //---------------------------------------------------Planes-----------------------------------------------------------------------------------------
    i=0;
    j=0;
    k=0;
    k=0;
    l=0;
    m=0;
    let bufferPlanSeñales = [];

    matrizCruces.forEach(()=>{
        matrizEstructura.forEach(()=>{
            matrizPlanes.forEach(()=>{
                bufferPlanSeñales[k] = '<Plan diffgr:id="Plan6" msdata:rowOrder="0" diffgr:hasChanges="inserted">'+'\n'+
                '<ID_Plan>'+matrizPlanes[k].numero+'</ID_Plan>'+'\n'+
                '<No>'+matrizPlanes[k].tiempos[l].numero+'</No>'+'\n'+
                '<Nombre>'+matrizPlanes[k].nombre+'</Nombre>'+'\n'+
                '<SS>'+matrizPlanes[k].SS+'</SS>'+'\n'+
                '<SC>'+matrizPlanes[k].SC+'</SC>'+'\n'+
                '<ID_Estructura>'+matrizEstructura[j].numero+'</ID_Estructura>'+'\n'+
                '<DC>'+matrizPlanes[k].DC+'</DC>'+'\n'+
                '<MaxVehiculos>'+matrizPlanes[k].max_vehiculos+'</MaxVehiculos>'+'\n'+
                '<Retardo>'+matrizPlanes[k].retencion+'</Retardo>'+'\n'+
                '</Plan>'+'\n'
                k=k+1;
                
                });
            j=j+1;
        });
        i=i+1;
    });

stringPlanSeñales = bufferPlanSeñales.toString();
//-----------------------------------------------------Equipo-----------------------------------------------------------------------------------



let timestampEquipo = archivojson.timestamp;
var fechaEquipo = new Date (timestampEquipo * 1000);
var añoEquipo = fechaEquipo.getFullYear();
var mesEquipo = fechaEquipo.getMonth();
var diaEquipo = fechaEquipo.getDay();
var horasEquipo =fechaEquipo.getHours();
var minutoEquipo = '0'+fechaEquipo.getMinutes();
var segundosEquipo = '0'+fechaEquipo.getSeconds();
timestampEquipoT = añoEquipo+'-'+mesEquipo+'-'+diaEquipo+'T'+horasEquipo+':'+minutoEquipo+':'+segundosEquipo;
bufferEquipo = '<Equipo diffgr:id="Equipo2" msdata:rowOrder="0" diffgr:hasChanges="inserted">'+'\n'+
'<ID_Equipo>'+archivojson.planeamiento+'</ID_Equipo>'+'\n'+
'<Cod>'+archivojson.codigo+'</Cod>'+'\n'+
'<Nombre>'+archivojson.nombre+'</Nombre>'+'\n'+
'<IP_Central>'+archivojson.servidor+'</IP_Central>'+'\n'+
'<ID_Depto>'+archivojson.id_depto+'</ID_Depto>'+'\n'+
'<ID_Ciudad>'+archivojson.id_ciudad+'</ID_Ciudad>'+'\n'+
'<Dir />'+'\n'+
'<ClaveTiempo>'+timestampEquipoT+'</ClaveTiempo>'+'\n'+// hacer la conversión
'<MiniCova>false</MiniCova>'+'\n'+ //me no saber
'</Equipo>'+'\n'

stringEquipo = bufferEquipo.toString();

//--------------------------------------------------Dias de la semana----------------------------------------------------------------------
         i=0;
         j=0;
         k=0;
         l=0;
         m=0;
let matrizDias = archivojson.cruces[i].estructuras[j].planes[k].horarios[l].dias;
        let bufferHorarios = [];
    matrizPlanes.forEach(StringHorarios=>{
        var diasDeSemana =[];
        var diasSemana = [];
        let diasBuffer = [];
        let Lunes
        let Martes
        let Miercoles
        let Jueves
        let Viernes
        let Sabado
        let Domingo
        let Festivo
        matrizDias.forEach(()=>{
            diasDeSemana[m] = matrizDias[m];
            diasBuffer[m] = diasDeSemana[m].split('.').pop();
            //console.log(.log(diasBuffer[m]);

            if(diasBuffer[m]=="Lunes"){
                Lunes=true;
            }else if(diasBuffer[m]=="Martes"){
               Martes = true;
            }else if(diasBuffer[m]=="Miercoles"){
                Miercoles=true;
            }else if(diasBuffer[m]=="Jueves"){
                Jueves=true;
            }else if(diasBuffer[m]==="Viernes"){
                Viernes = true;
            }else if(diasBuffer[m]==="Sabado"){
                Sabado=true;
            }else if(diasBuffer[m]==="Domingo"){
                Domingo=true;
            }else if(diasBuffer[m]==="Festivo"){
                Festivo=true;
            }
            m=m+1;
        })
        let timestampInicio = matrizPlanes[l].horarios[k].timestamp_inicio;
        let timestampFin = matrizPlanes[l].horarios[k].timestamp_fin
        var fechaInicio = new Date (timestampInicio * 1000);
        var fechaFin = new Date (timestampFin * 1000);
        var añoInicio = fechaInicio.getFullYear();
        var añoFin = fechaFin.getFullYear();
        var mesInicio = fechaInicio.getMonth();
        var mesFin = fechaFin.getMonth();
        var diaInicio = fechaInicio.getDay();
        var diaFin = fechaFin.getDay();
        var horaInicio =fechaInicio.getHours();
        var horaFin =fechaFin.getHours();
        var minutoInicio = '0'+fechaInicio.getMinutes();
        var minutoFin = '0'+fechaFin.getMinutes();
        var segundoInicio = '0'+fechaInicio.getSeconds();
        var segundoFin = '0'+fechaFin.getSeconds();
        tiempoInicio = añoInicio+'-'+mesInicio+'-'+diaInicio+'T'+horaInicio+':'+minutoInicio+':'+segundoInicio;
        tiempoFin = añoFin+'-'+mesFin+'-'+diaFin+'T'+horaFin+':'+minutoFin+':'+segundoFin;
            bufferHorarios = '<HPlan diffgr:id="HPlan10" msdata:rowOrder="1" diffgr:hasChanges="inserted">'+'\n'+
            '<ID_HPlan>'+matrizPlanes[l].numero+'</ID_HPlan>'+'\n'+
            '<H_Inicio>'+tiempoInicio+'</H_Inicio>'+'\n'+
            '<H_Fin>'+tiempoFin+'</H_Fin>'+'\n'+
            '<ID_Plan>'+matrizPlanes[l].nombre+'</ID_Plan>'+'\n'+
            '<Lunes>'+Lunes+'</Lunes>'+'\n'+
            '<Martes>'+Martes+'</Martes>'+'\n'+
            '<Miercoles>'+Miercoles+'</Miercoles>'+'\n'+
            '<Jueves>'+Jueves+'</Jueves>'+'\n'+
            '<Viernes>'+Viernes+'</Viernes>'+'\n'+
            '<Sabado>'+Sabado+'</Sabado>'+'\n'+
            '<Domingo>'+Domingo+'</Domingo>'+'\n'+
            '<Festivo>'+Festivo+'</Festivo>'+'\n'+
          '</HPlan>'+'\n'
        })

stringHorarios = bufferHorarios.toString();
//----------------------------------------------Tiempos intermedios-----------------------------------------------------------------------------------------
i=0;
j=0;
k=0;
l=0;
matrizTiemposIntermedios = archivojson.cruces[i].estructuras[j].tiempos_intermedios
bufferTiemposIntermedios = [];
matrizCruces.forEach(()=>{
    matrizEstructura.forEach(()=>{
        matrizTiemposIntermedios.forEach(()=>{

            bufferTiemposIntermedios[k] ='<TIntermedio diffgr:id="TIntermedio1" msdata:rowOrder="0" diffgr:hasChanges="inserted">'+'\n'+
            '<ID_TIntermedio>'+matrizEstructura[j].tiempos_intermedios[k].numero+'</ID_TIntermedio>'+'\n'+
            '<ID_Grupo1>'+matrizEstructura[j].tiempos_intermedios[k].grupo1+'</ID_Grupo1>'+'\n'+
            '<ID_Grupo2>'+matrizEstructura[j].tiempos_intermedios[k].grupo2+'</ID_Grupo2>'+'\n'+
            '<Tiempo>'+matrizEstructura[j].tiempos_intermedios[k].tiempo+'</Tiempo>'+'\n'+
            '<BTiempo>true</BTiempo>'+'\n'+
            '<ID_Estructura>'+matrizEstructura[j].numero+'</ID_Estructura>'+'\n'+
            '</TIntermedio>'+'\n'

            k=k+1;

        });
        j=j+1;
    });
    i=i+1;
});
stringTiemposIntermedios = bufferTiemposIntermedios.toString();
//-------------------------------------------------Estructura----------------------------------------------------------
i=0;
j=0;
bufferEstructura = [];
matrizCruces.forEach(()=>{
    matrizEstructura.forEach(()=>{
        if(matrizEstructura.TRA==""){
            var TRA = false
        }else if(matrizEstructura.TA==""){
            var TA = false
        };
        bufferEstructura [j]= '<Estructura diffgr:id="Estructura'+j+'" msdata:rowOrder="'+j+'" diffgr:hasChanges="inserted">'+'\n'+
                '<ID_Estructura>'+'0'+j+'</ID_Estructura>'+'\n'+
                '<No>'+(j+1)+'</No>'+'\n'+
                '<Nombre>'+matrizEstructura[j].nombre+'</Nombre>'+'\n'+
                '<TRA>'+matrizEstructura[j].TRA+'</TRA>'+'\n'+
                '<BTRA>'+TRA+'</BTRA>'+'\n'+
                '<TA>'+matrizEstructura[j].TA+'</TA>'+'\n'+
                '<BTA>'+TA+'</BTA>'+'\n'+
                '<ID_Cruce>'+matrizCruces[i].numero+'</ID_Cruce>'+'\n'+
            '</Estructura>'+'\n'
            j=j+1;
        });
        i=i+1;
});
stringEstructura = bufferEstructura.toString();
//-------------------------------------------------Versión---------------------------------------------------------------
stringConfiguraciones =     
    '<Configuracion diffgr:id="Configuracion1" msdata:rowOrder="0" diffgr:hasChanges="inserted">'+'\n'+
    '<Version>2.5</Version>'+'\n'+
    '</Configuracion>'+'\n';
    
//-----------------------------------------------------Grupo inventario----------------------------------------------------------------------------
matrizTarjetas = [];
for(i=0;i<16;i++){
    matrizTarjetas[i]=i
};
i=0;
j=0;
let bufferGrupoInventarios=[];
matrizTarjetas.forEach(()=>{
bufferGrupoInventarios[i] = '<GrupoInventario diffgr:id="GrupoInventario'+(i+1)+'" msdata:rowOrder="'+i+'" diffgr:hasChanges="inserted">'+'\n'+
        '<ID_Tarjeta>'+matrizTarjetas[i]+'</ID_Tarjeta>'+'\n'+
        '<Presente>false</Presente>'+'\n'+
        '<ID_Equipo>'+archivojson.codigo+'</ID_Equipo>'+'\n'+
        '<Grupos>Grupos: '+(j+1)+' y '+(j+2)+'</Grupos>'+'\n'+
        '<PrimerGrupo>'+j+'</PrimerGrupo>'+'\n'+
      '</GrupoInventario>'+'\n';
      j=j+2;
      i=i+1;
    
      
    });
    stringGrupoInventarios = bufferGrupoInventarios.toString();








//---------------------------------------------------------------------------------------------------------------------------------------
StringDeclaraciones3 = 
'      </DSCronos>'+'\n'+
'   </diffgr:diffgram>'+'\n'+
'</DSCronos>';
let stringCronos = `${StringDeclaraciones1}
${stringDeclaraciones2}
${stringCruces}
${stringGruposAmigos}
${stringGrupos}
${stringTPlanes}
${stringPlanSeñales}
${stringEquipo}
${stringHorarios}
${stringTiemposIntermedios}
${stringEstructura}
${stringConfiguraciones}
${stringGrupoInventarios}
${StringDeclaraciones3}`;
console.log(stringCronos.replaceAll(",",""))
var nombreArchivo = prompt("Guardar como:");

if (nombreArchivo == null || nombreArchivo == "") {
  txt = "User cancelled the prompt.";
} else {
  nombre=nombreArchivo.toString()+'.crs';
}
var jsonBlob = new Blob([stringCronos.replace(",","+'\n'+")], { type: 'application/javascript;charset=utf-8' });
saveAs(jsonBlob,nombre);

};